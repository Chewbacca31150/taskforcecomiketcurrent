import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Order } from 'src/app/models/order';
import { ImagezoomComponent } from '../imagezoom/imagezoom.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent {
  @Input() order: Order;
  @Output() onGoToDetails = new EventEmitter();

  constructor(private modalController: ModalController) { }

  preventSingleClick = false;
  timer: any;
  delay: number;

  goToDetails() {
    this.onGoToDetails.emit();
  }

  async viewImage(src: string) {
    const modal = await this.modalController.create({
      component: ImagezoomComponent,
      componentProps: {
        imgSource: src,
      },
      cssClass: 'modal-fullscreen',
      keyboardClose: true,
      showBackdrop: true
    });
    return await modal.present();
  }
}
