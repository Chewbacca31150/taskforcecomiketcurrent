import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import * as _ from 'lodash';

@Component({
  selector: 'app-imagezoom',
  templateUrl: './imagezoom.component.html',
  styleUrls: ['./imagezoom.component.scss'],
})
export class ImagezoomComponent implements OnInit {
    @ViewChild('slider', { read: ElementRef })slider: ElementRef;
    imgSource: any;

    slideOpts = {
      centeredSlides: 'true',
      zoom: {
        toggle: true
      }
    };

    constructor(private modalController: ModalController) {}

    ngOnInit() {}

    closeModal() {
      this.modalController.dismiss();
    }

    zoom(zoomIn: boolean) {
        const zoom = this.slider.nativeElement.swiper.zoom;
        if (zoomIn) {
          zoom.in();
        } else {
          zoom.out();
        }
      }
}
