import { ItemComponent } from './item/item.component';
import { DayComponent } from './day/day.component';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DetailsModal } from './details/details.modal';
import { IonicImageLoader } from 'ionic-image-loader';
import { ImagezoomComponent } from './imagezoom/imagezoom.component';
import { DoneComponent } from './done/done.component';


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    IonicImageLoader
  ],
  declarations: [
  ItemComponent, DayComponent, DetailsModal, ImagezoomComponent, DoneComponent
  ],
  exports: [
    ItemComponent, DayComponent, DetailsModal, ImagezoomComponent, DoneComponent
  ], 
  entryComponents: [
    DetailsModal, ImagezoomComponent, DoneComponent
  ]
})
export class SharedModule { }