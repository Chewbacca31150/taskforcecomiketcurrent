import { Component, Input, ChangeDetectorRef } from '@angular/core';
import { Order } from 'src/app/models/order';
import { ModalController, LoadingController } from '@ionic/angular';
import { DetailsModal } from '../details/details.modal';
import { ImagezoomComponent } from '../imagezoom/imagezoom.component';
import { SpreadSheetService } from 'src/app/services/spreadsheet.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.scss'],
})
export class DayComponent {
  @Input() typeWanted: string;
  sheetToUse: string;

  orders: Order[];
  grid: Order[][];
  error: string;
  constructor(
    private spreadSheetService: SpreadSheetService,
    private modalController: ModalController,
    private loadingController: LoadingController,
    private utilsService: UtilsService,
    private cdr: ChangeDetectorRef) { }

  setOrders(orders: Order[]) {
    this.orders = orders;
  }

  setSheetToUse(sheetToUse: string) {
    this.sheetToUse = sheetToUse;
  }

  initPage() {
    if (!this.orders) this.loadFirstTime();
    this.loadData();
  }

  /**
   * Displays the spinner
   */
  loadFirstTime() {
    let loading: Promise<HTMLIonLoadingElement> = null;
    loading = this.loadingController.create({
      spinner: "bubbles",
    });
    loading.then(load => load.present());
    this.getData(loading);
  }

  /**
   * Retrieves data each 20sec
   */
  loadData() {
    const id = window.setInterval(_ => this.getData(), 10000);
    this.utilsService.addInterval({ type: 'app', id: id });
  }

  /**
   * Get data, refresh if needed
   */
  getData(loading?: any): Promise<any> {
    if (this.typeWanted === 'currents') {
      return this.spreadSheetService.getOrdersByCurrent(this.sheetToUse).then((res: Order[]) => {
        this.removeLoader(loading);
        if (res && JSON.stringify(this.orders) === JSON.stringify(res)) return;
        const neworders = res.sort((a, b) => a.current - b.current);
        this.orders = neworders;
        this.reloadCdr(loading);
        if (!this.orders) this.error = 'No entry found yet';
        return this.orders;
      }, _ => {
        this.error = 'Network error';
        this.removeLoader(loading);
      });
    } else {
      return this.spreadSheetService.getOrdersByOwner().then((res: Order[]) => {
        this.removeLoader(loading);
        const neworders = res
          .filter((order: Order) => this.getPreviousOrNextFilter(order.status, order.current))
          .sort((a, b) => this.typeWanted === 'previous' ? b.current - a.current : a.current - b.current)
          .slice(0, 10);
        if (neworders && JSON.stringify(this.orders) === JSON.stringify(neworders)) return;
        if (!this.orders) this.error = 'No entry found yet';
        this.orders = neworders;
        this.reloadCdr(loading);
        return this.orders;
      }, _ => {
        this.error = 'Network error';
        this.removeLoader(loading);
      });
    }
  }

  /**
   * Remove the loader if needed
   * @param loading 
   */
  removeLoader(loading) {
    if (loading !== undefined && loading !== null) loading.then(load => load.remove());
  }

  /**
   * Force reload scope if needed
   * @param loading
   */
  reloadCdr(loading) {
    if (loading !== undefined && loading !== null) this.cdr.detectChanges();
  }

  /**
   * Previous or next
   * @param status
   * @param previousOrNext
   */
  getPreviousOrNextFilter(status: string, current: number) {
    switch (this.typeWanted) {
      case 'next':
        return current > 0;
      case 'previous':
        return current < 0 && status !== 'postponed';
      case 'postponed':
        return current < 0 && status === 'postponed';
      default:
        console.error('status not implemented', this.typeWanted);
    }
  }

  updateCurrent(order: Order) {
    this.spreadSheetService.getOrdersByOwner().then((data: Order[]) => {
      const next = data.find(or => or.current === order.current);
      const orderFound = order.current;
      let previousEl: Order = null;
      const dataToPost = data.map((element: Order) => {
        // find selected order
        if(element.current === 0) {
          previousEl = element;
          return next;
        } else {
          if (element.current > 0 && element.current <= orderFound) {
            const toReturn = previousEl;
            previousEl = element;
            return toReturn;
          }
        }
        return element;
      });
      this.spreadSheetService.postDatas(dataToPost).then(_ => this.loadFirstTime(), err => console.error(JSON.stringify(err, null, 4)));

    });

  }

  /**
   * Go to details
   * @param order
   */
  async goToDetails(order: Order) {
    const sheetToUse = this.sheetToUse;
    const modal = await this.modalController.create({
      component: DetailsModal,
      componentProps: {
        order,
        sheetToUse
      }
    });
    modal.onDidDismiss().then(_ => this.loadData());
    modal.present()
      .then(() => {
        this.utilsService.clearIntervals('app');
      });
  }

  async viewImage(src: string) {
    const modal = await this.modalController.create({
      component: ImagezoomComponent,
      componentProps: {
        imgSource: src,
      },
      cssClass: 'modal-fullscreen',
      keyboardClose: true,
      showBackdrop: true
    });
    return await modal.present();
  }
}
