import { Component, OnInit, Input } from '@angular/core';
import { AlertController, PopoverController } from '@ionic/angular';
import { SpreadSheetService } from 'src/app/services/spreadsheet.service';
import { Order } from 'src/app/models/order';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-done',
  templateUrl: './done.component.html',
  styleUrls: ['./done.component.scss'],
})
export class DoneComponent implements OnInit {
  @Input() current: Order;

  constructor(
    private alertController: AlertController,
    private spreadsheetService: SpreadSheetService,
    private popoverController: PopoverController,
    private utilsService: UtilsService) { }

  ngOnInit() {
    this.utilsService.clearIntervals('app');
  }
  /**
   * Display alert done
   * @param type type of done
   */
  displayAlertDone(type) {

    let message: string;
    if (type === 'done') {
      message = 'Item(s) bought';
    } else if (type === 'missing') {
      message = 'Some items were missing';
    } else if (type === 'kanbai') {
      message = 'Kanbai';
    } else if (type === 'postponed') {
      message = 'Postponed';
    } else if (type === 'cancelled') {
      message = 'Cancelled';
    }

    this.alertController.create({
      header: type,
      message,
      inputs: [
        {
          name: 'note',
          type: 'text',
          placeholder: 'Enter a note'
        },
        {
          name: 'priceReal',
          type: 'number',
          placeholder: 'Enter a price'
        }

      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            if (data.note) this.current.noteDone = data.note;
            if (data.priceReal) this.current.priceReal = data.priceReal;
            if (data.whoGetsIt) this.current.whoGetsIt = this.utilsService.getAuthInstance().getUser().displayName;
            this.current.status = type;
            this.current.dateDone = new Date();
            this.spreadsheetService.incrementDone().then(_ => {
              this.spreadsheetService.postData(this.current).then(_=> console.log('done'), err => console.log(JSON.stringify(err, null, 4)));
            });
            /*this.spreadsheetService.getOrdersByOwner().then((data: Order[]) => {
              let dataFormat = data.map(order => {
                if (order.circle === this.current.circle && order.what === this.current.what) {
                  order = this.current;
                }
                order.current = order.current - 1;
                return order;
              });
              this.current = dataFormat.find(d => d.current === 0);
              this.spreadsheetService.postDatas(dataFormat).then(_ => {
              }, err => console.error(JSON.stringify(err, null, 4)));
            });*/
          }
        }
      ]
    }).then(alert => {
      alert.present();
      alert.onDidDismiss().then(_ => this.popoverController.dismiss());
    });
  }
}
