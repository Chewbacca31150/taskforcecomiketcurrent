import { Component, OnInit, Input } from '@angular/core';
import { Order } from 'src/app/models/order';
import { ModalController } from '@ionic/angular';
import * as _ from 'lodash';
import { SpreadSheetService } from 'src/app/services/spreadsheet.service';
@Component({
  selector: 'app-details',
  templateUrl: './details.modal.html',
  styleUrls: ['./details.modal.scss'],
})
export class DetailsModal implements OnInit {
  @Input() order: Order;
  @Input() sheetToUse: string;

  constructor(private spreadSheetService: SpreadSheetService, private modalController: ModalController) { }

  ngOnInit() {
  }

  /**
   * saves an order
   * @param order
   */
  save(order: Order) {
    this.spreadSheetService.postData(order, this.sheetToUse).then(data => {
      this.dismiss();
    }, err => console.error(err));
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
