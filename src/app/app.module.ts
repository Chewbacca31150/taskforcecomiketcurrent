import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { SharedModule } from './components/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { IonicImageLoader } from 'ionic-image-loader';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { AuthAndroidService } from './services/auth.android.service';
import { AuthWebService } from './services/auth.web.service';
import { AuthAbstractService } from './services/auth.abstract.service';
import { UtilsService } from './services/utils.service';
import { SpreadSheetService } from './services/spreadsheet.service';

const firebaseConfig = {
  apiKey: "AIzaSyCRC9eka2ujjo51e5q70P4UGhSpzj1_tJ0",
  authDomain: "task-force-current.firebaseapp.com",
  databaseURL: "https://task-force-current.firebaseio.com",
  projectId: "task-force-current",
  storageBucket: "task-force-current.appspot.com",
  messagingSenderId: "438944116448",
  appId: "1:438944116448:web:5539ed9302a9a1a5",
  clientId: "438944116448-2j2gl3a4a3pe71grp37cie67mis9112s.apps.googleusercontent.com"
};
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, FormsModule, ReactiveFormsModule, 
    HttpClientModule, IonicModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    IonicImageLoader.forRoot(),
    AngularFireAuthModule,
    AppRoutingModule,
    SharedModule],
  providers: [
    GooglePlus,
    StatusBar,
    SplashScreen,
    AuthAndroidService,
    AuthWebService,
    UtilsService,
    SpreadSheetService,
    WebView,
    AppVersion,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
