import { Component } from '@angular/core';

import { Platform, MenuController} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { ImageLoaderConfigService } from 'ionic-image-loader';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { UtilsService } from './services/utils.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
})
export class AppComponent {
  user: any = {};
  versionNumber;
  constructor(
    private platform: Platform,
    public utilsService: UtilsService,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menu: MenuController,
    private route: Router,
    private imageLoaderConfig: ImageLoaderConfigService,
    public appVersion: AppVersion
  ) {
    this.initializeApp();
  }

  goTo() {
    this.menu.toggle();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.imageLoaderConfig.enableSpinner(false);
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      if(this.utilsService.isCordova()) this.appVersion.getVersionNumber().then(ver => this.versionNumber = ver);
      this.user = this.utilsService.getAuthInstance().getUser();
      if(this.user != null) {
        this.route.navigate(['/tabs']);
      } else {
        this.route.navigate(['/home']);
      }
    });
  }

  signOut() {
    this.utilsService.getAuthInstance().logOutGoogle().subscribe(_ => {
     this.reloadPage();
   });
  }

  reloadPage() {
    setTimeout(_ => location.reload(), 200);
  }
}
