export type Object = { [key: string]: any }
export type GoogleAuthResponse = {
    accessToken: string
    displayName: string
    email: string
    expires: number
    expires_in: number
    familyName: string
    givenName: string
    userId: string
    refreshToken: string
    serverAuthCode: string
    photoURL: string
    stsTokenManager: any
}
