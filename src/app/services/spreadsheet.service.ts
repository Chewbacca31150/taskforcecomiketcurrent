import { Injectable } from '@angular/core';
import { AuthAbstractService } from './auth.abstract.service';
import { ConfSheet } from '../models/confSheet';
import { Order } from '../models/order';
import { Configuration } from '../models/configuration';
import { UtilsService } from './utils.service';

@Injectable()
export class SpreadSheetService {
    confGetCommon: ConfSheet;
    spreadSheetId: string;

    constructor(private utilsService: UtilsService) {
        // conf spreadsheet
        this.confGetCommon = {
            sheet: 'J1',
            startingRange: 2,
            startingColumn: 'A',
            endRange: 'P'
        };
        // spreadsheet id
        this.spreadSheetId = '1Fnp3fvF_V9C_BACSD5dpkbzgfpJGNBJXoVxwtY5Qvfk';
        this.utilsService.getAuthInstance().initClient()
    }
    /**
     * Retrieve orders in owner spreadsheet
     */
    getOrdersByOwner(): any {
        return this.getConfig().then(_ => {
            return this.getAll()
                .then((orders) => {
                    if (!this.utilsService.getAuthInstance().getUser()) return;
                    return orders;
                });
        });
    }

    /**
     * 
     * Retrieve orders filtered by current
     */
    getOrdersByCurrent(sheetToUser: string): any {
        return this.getAll(sheetToUser)
            .then((orders) => {
                if (!this.utilsService.getAuthInstance().getUser()) return;
                return orders
                    .filter(order => order.current >= 0 && order.current <= 3);
            }, err => console.log(err));
    }


    /**
     * Get current spreadsheet
     * @param conf 
     */
    getCurrentSpreadSheet(conf: ConfSheet): any {
        return this.utilsService.getAuthInstance().getGapi().client.sheets.spreadsheets.values.get({
            spreadsheetId: this.spreadSheetId,
            range: conf.sheet + '!' + conf.startingColumn + conf.startingRange
                + ':' + conf.endRange
        }).then(res => {
            const rows = res.result.values;
            if (rows.length) {
                return rows;
            } else {
                return "no data";
            }
        }).catch(err => this.utilsService.getAuthInstance().updateToken());
    }

    /**
     * Update spreadsheet
     * @param data 
     * @param range 
     */
    postData(data: Order, sheetName?: string, retry?: boolean) {
        const confSpreadsheet = this.confGetCommon;
        if (sheetName != null) confSpreadsheet.sheet = sheetName;
        const arr = this.formatEdit(data);
        const rangeStr = confSpreadsheet.sheet + '!' + data.row.charAt(0) + +data.row.substr(1) + ':' + confSpreadsheet.endRange;
        return this.utilsService.getAuthInstance().getGapi().client.sheets.spreadsheets.values.update({
            spreadsheetId: this.spreadSheetId,
            range: rangeStr,
            valueInputOption: 'RAW',
            responseDateTimeRenderOption: 'SERIAL_NUMBER',
            includeValuesInResponse: true,
            values: [arr]
        }).then(data => data, err => {
            this.utilsService.getAuthInstance().updateToken();
            if (retry) {
                setTimeout(() => this.postData(data, sheetName, false), 200);
            } else {
                alert("Error: " + JSON.stringify(err, null, 2));
            }
        });
    }

    /**
     * Increment done
     * @param datas 
     * @param retry 
     */
    incrementDone() {
        const confSpreadsheet = this.confGetCommon;
        const rangeStr = this.confGetCommon.sheet + '!P1:P1';
        return this.utilsService.getAuthInstance().getGapi().client.sheets.spreadsheets.values.get({
            spreadsheetId: this.spreadSheetId,
            range: rangeStr
        }).then(data => {
            const done = [+data.result.values[0] + 1];
            return this.utilsService.getAuthInstance().getGapi().client.sheets.spreadsheets.values.update({
                spreadsheetId: this.spreadSheetId,
                range: rangeStr,
                valueInputOption: 'RAW',
                responseDateTimeRenderOption: 'SERIAL_NUMBER',
                includeValuesInResponse: true,
                values: [done]
            }).then(data => console.log(data), err => console.error(err));
        })
    }
    
    /**
   * Update spreadsheet
   * @param data 
   * @param range 
   */
    postDatas(datas: Order[], retry?: boolean) {
        const arr = datas.map(data => this.formatEdit(data));
        const rangeStr = this.confGetCommon.sheet + '!' + this.confGetCommon.startingColumn + this.confGetCommon.startingRange
            + ':' + this.confGetCommon.endRange;
        return this.utilsService.getAuthInstance().getGapi().client.sheets.spreadsheets.values.update({

            spreadsheetId: this.spreadSheetId,
            range: rangeStr,
            valueInputOption: 'RAW',
            responseDateTimeRenderOption: 'SERIAL_NUMBER',
            includeValuesInResponse: true,
            values: arr
        }).then(data => data, err => {
            this.utilsService.getAuthInstance().updateToken();
            if (retry) {
                setTimeout(() => this.postDatas(datas, false), 200);
            } else {
                alert("Error: " + JSON.stringify(err, null, 2));
            }
        });
    }

    /**
     * Retrieves the config from the config tab
     */
    getConfig(): any {
        return this.getCurrentSpreadSheet({
            endRange: 'B',
            sheet: 'Conf',
            startingRange: 2,
            startingColumn: 'A'
        }).then(data => {
            if (!data) return;
            const configuration: Configuration[] = [];
            let sheetFound = false;
            data.forEach(element => {
                const d = new Date(element[1]);
                if (d == null) return;
                if (d.toLocaleDateString() === new Date().toLocaleDateString()) {
                    sheetFound = true;
                    this.confGetCommon.sheet = element[0] + "_" + this.utilsService.getAuthInstance().getUser().displayName;
                }
                configuration.push({
                    type: element[0],
                    date: d
                });
            });
            if (!sheetFound) {
                if (data[data.length - 1][1] < new Date()) {
                    this.confGetCommon.sheet = data[data.length - 1][0] + "_" + this.utilsService.getAuthInstance().getUser().displayName;
                } else {
                    this.confGetCommon.sheet = data[0][0] + "_" + this.utilsService.getAuthInstance().getUser().displayName;
                }
            }
            return configuration;
        }, (err) => console.log(err));
    }

    getAllSheets(): any {
        return this.getConfig().then(_ => {
            return this.utilsService.getAuthInstance().getGapi().client.sheets.spreadsheets.get({
                spreadsheetId: this.spreadSheetId,
                includeGridData: false,
                ranges: []
            }).then((data) => {
                return data.result.sheets
                    .map(sheet => sheet.properties.title)
                    .filter(sheet => sheet.match(/J.?_(.*)/gm));
            }, (err => this.utilsService.getAuthInstance().updateToken()));
        });
    }


        /**
     * Map order to object to send to spreadsheet
     * @param data 
     */
    private formatEdit(data: Order): any {
        return [data.hall,
        data.booth,
        data.circle,
        data.what,
        data.for,
        data.whoGetsIt,
        data.priceExpected,
        data.website,
        data.note,
        data.priority,
        data.image,
        data.priceReal,
        data.status,
        data.noteDone,
        data.dateDone
        ];
    }

    /**
     * Get all orders from spreadsheet
     */
    private getAll(sheetName?: string): Promise<Order[]> {
        const confSpreadsheet = this.confGetCommon;
        if (sheetName != null) confSpreadsheet.sheet = sheetName;

        return this.getCurrentSpreadSheet(confSpreadsheet)
            .then((data) => {
                if (!data) return;
                const orders: Order[] = [];
                let i: number = confSpreadsheet.startingRange;
                data.forEach((element) => {
                    const order: Order = {
                        hall: element[0],
                        booth: element[1],
                        circle: element[2],
                        what: element[3],
                        for: element[4],
                        whoGetsIt: element[5],
                        priceExpected: parseInt(element[6]),
                        website: element[7],
                        note: element[8],
                        priority: parseInt(element[9]),
                        image: element[10],
                        priceReal: parseInt(element[11]),
                        status: element[12],
                        noteDone: element[13],
                        dateDone: null,
                        current: parseInt(element[15]),
                        row: this.confGetCommon.startingColumn + i
                    };
                    i++;
                    orders.push(order);
                });
                return orders;
            }, (err) => console.log(err));
    }
}