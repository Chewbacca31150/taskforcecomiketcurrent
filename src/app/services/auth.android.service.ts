import { AuthAbstractService } from './auth.abstract.service';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Platform } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { GoogleAuthResponse } from '../app.types';

declare var gapi;

@Injectable()
export class AuthAndroidService extends AuthAbstractService {

  constructor(
    protected google: GooglePlus,
    protected platform: Platform,
    protected afAuth: AngularFireAuth,
    private http: HttpClient) {
    super(google, platform, afAuth);

  }

  initClient() {
    const client = 'client:auth2';
    gapi.load(client, () => {
      gapi.client.init(this.confGapi).then(() => {
        if (this.getUser() !== null) {
          this.getAccessToken().subscribe(res => {
            gapi.client.setToken({ access_token: res['access_token'] });
            gapi.client.load('sheets', 'v4', () => {
            }), err => console.log(err)
          }, err => console.log(err))
        }
      });
    })
  }

  public updateToken() {
    this.getAccessToken()
      .subscribe(res => {
        gapi.client.setToken({ access_token: res['access_token'] });
        gapi.client.load('sheets', 'v4', () => {
        }), err => alert(JSON.stringify(err, null, 2));
      });
  }

  public googleAuthentication(): any {
    this.initClient();
    let promise: Promise<any> = null;
    if (!gapi) {
      setTimeout(() => { promise = this.auth(); }, 2000);
    } else {
      promise = this.auth();
    }
    promise.then(succ => JSON.stringify(succ), err => alert(JSON.stringify(err, null, 2)));
  }

  public getGapi() {
    return gapi;
  }

  getAccessToken() {
    return this.callOauthToken({
      refresh_token: this.getUser().refreshToken,
      client_secret: this.clientSecret,
      grant_type: 'refresh_token',
      client_id: this.clientId
    });
  }

  refreshToken(serverAuthCode: string) {
    return this.callOauthToken({
      code: serverAuthCode,
      client_secret: this.clientSecret,
      grant_type: 'authorization_code',
      client_id: this.clientId
    });
  }

  private callOauthToken(serialized) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    headers.append('Access-Control-Allow-Credentials', 'true');
    headers.append('Access-Control-Allow-Origin', '*');
    let options = { headers: headers };
    return this.http
      .post("https://www.googleapis.com/oauth2/v4/token", serialized, options);
  }

  /**
   * Init Google Authentication
   */
  auth(): Promise<any> {
    return this.google.login({
      webClientId: this.clientId,
      offline: true,
      scopes: 'profile email https://www.googleapis.com/auth/spreadsheets'
    }).then(user => {
      const { idToken, accessToken } = user;
      const credential = accessToken ? this.getFirebase().auth.GoogleAuthProvider
        .credential(idToken, accessToken) : this.getFirebase().auth.GoogleAuthProvider
          .credential(idToken);
      return this.refreshToken(user.serverAuthCode).subscribe(r => {
        const userToSave: GoogleAuthResponse = user;
        userToSave.refreshToken = r['refresh_token'];
        this.setUser(userToSave);
        return this.afAuth.auth.signInWithCredential(credential)
          .then(data => {
            setTimeout(_ => location.reload(), 200);
            return userToSave;
          }).catch(error => {
          });
      });
    })
  }

  public logOutGoogle(): Observable<any> {
    return Observable.create(observer => {
      this.afAuth.auth.signOut().then(_ => {
        observer.next({});
        this.removeUser();
      }).catch(err => {
        observer.next({});
        this.removeUser();
      });
    });
  }
}
