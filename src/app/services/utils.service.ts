import { Platform } from '@ionic/angular';
import { AuthAbstractService } from './auth.abstract.service';
import { AuthWebService } from './auth.web.service';
import { AuthAndroidService } from './auth.android.service';
import { Injectable } from '@angular/core';
import { Interval } from '../models/interval';

@Injectable()
export class UtilsService {
    authInstance: AuthAbstractService;
    intervals: Interval[] = [];
    constructor(protected authAndroid: AuthAndroidService, protected authWeb: AuthWebService, protected platform: Platform) {
    }

    isCordova(): boolean {
        return (this.platform.is('cordova')) ? true : false;
    }

    getAuthInstance() {
        return this.isCordova() ? this.authAndroid : this.authWeb;
    }

    clearIntervals(type: String) {
        this.intervals
        .filter((interval: Interval) => interval.type === type)
        .forEach(interval => clearInterval(interval.id));
        this.intervals = this.intervals.filter((interval: Interval) => interval.type !== type);
    }

    addInterval(interval: Interval) {
        this.intervals.push(interval);
    }

    clearIntervalById(id: number) {
        clearInterval(id);
        this.intervals = this.intervals.filter(i => i.id !== id);
    }
}
