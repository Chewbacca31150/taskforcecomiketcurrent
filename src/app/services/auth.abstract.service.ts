

import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { Platform } from '@ionic/angular';
import { GoogleAuthResponse } from '../app.types';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import * as firebase from 'firebase';

/**
 * @author Quentin San José
 * @description Authentication service for checking and validating user credentials + spreadsheet
 */
@Injectable()
export abstract class AuthAbstractService {
  private readonly USER: string = 'USER';
  calendarItems: any[];
  confGapi: any;
  spreadSheetId = '';
  clientId: string;
  clientSecret: string;

  constructor(protected google: GooglePlus,
    protected platform: Platform,
    protected afAuth: AngularFireAuth) {
    this.clientId = '438944116448-2j2gl3a4a3pe71grp37cie67mis9112s.apps.googleusercontent.com';
    this.clientSecret = '2g4vycS3qUJj-NOadnZUZBnX';
    // spreadsheet id
    this.spreadSheetId = '1Fnp3fvF_V9C_BACSD5dpkbzgfpJGNBJXoVxwtY5Qvfk';
    // conf gapi
    this.confGapi = {
      scope: 'https://www.googleapis.com/auth/spreadsheets',
      apiKey: 'AIzaSyCRC9eka2ujjo51e5q70P4UGhSpzj1_tJ0',
      clientId: this.clientId,
      discoveryDocs: ['https://sheets.googleapis.com/$discovery/rest?version=v4']
    };
  }

  getFirebase() {
    return firebase;
  }

  updateToken() {
  }

  /**
   * Initialize the Google API client with desired scopes
   */
  initClient() {
    // overrided
  }

  getGapi() {
    return null;
  }

  updateSigninStatus(isSignedIn: boolean) {
    if (isSignedIn) {
      console.log('signed in');
    } else {
      console.error('not signed in');
    }
  }

  /**
   * Init Google Authentication
   */
  public googleAuthentication(): Observable<GoogleAuthResponse> {
    return null;
  }

  /**
   * Retrieves user
   */
  public getUser(): GoogleAuthResponse {
    return JSON.parse(localStorage.getItem(this.USER));
  }

  /**
   * Logs user out from the connection
   */
  public logOutGoogle(): Observable<any> {
    return null;
  }

  /**
   * Sets user in storage
   * @param user 
   */
  protected setUser(user) {
    localStorage.setItem(this.USER, JSON.stringify(user));
  }

  /**
   * Clear user info
   */
  protected removeUser() {
    localStorage.removeItem(this.USER);
  }
}
