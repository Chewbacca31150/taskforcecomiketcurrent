import { AuthAbstractService } from './auth.abstract.service';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Platform } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { GoogleAuthResponse } from '../app.types';
import * as firebase from 'firebase';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
declare var gapi;

@Injectable()
export class AuthWebService extends AuthAbstractService {
    constructor(google: GooglePlus, platform: Platform, afAuth: AngularFireAuth, public http: HttpClient) {
        super(google, platform, afAuth);
    }

    initClient() {
        const client = 'client';
        gapi.load(client, () => {
            gapi.client.init(this.confGapi).then(() => {
                gapi.client.load('sheets', 'v4', () => {
                    gapi.auth2.getAuthInstance().isSignedIn.listen(this.updateSigninStatus);
                    // Handle the initial sign-in state.
                    this.updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
                }), err => console.error(err)
            });
        });
    }

    getGapi() {
        return gapi;
    }

    public googleAuthentication(): any {
        this.initClient();
        let promise: Promise<any>;
        if (!gapi || !gapi.auth2) {
            setTimeout(() => { promise= this.auth(); }, 2000);
        } else {
            promise =this.auth();
        }
        promise.then(succ => JSON.stringify(succ), err=> JSON.stringify(err));
    }

    public auth(): Promise<any> {
        const googleAuth = gapi.auth2.getAuthInstance();
        return googleAuth.signIn(this.confGapi).then(googleUser => {
            const token = googleUser.getAuthResponse().id_token;
            const credential = firebase.auth.GoogleAuthProvider.credential(token);
            return this.afAuth.auth.signInAndRetrieveDataWithCredential(credential).then(data => {
                this.setUser(data.user);
                setTimeout(_ => location.reload(), 200);
                return data;
            });
        }, err => console.log(err));
    }

    public logOutGoogle(): Observable<any> {
        const googleAuth = gapi.auth2.getAuthInstance();
        return Observable.create(observer => {
            googleAuth.signOut().then(googleUser => {
                observer.next({});
                this.removeUser();
            });
        });
    }
}
