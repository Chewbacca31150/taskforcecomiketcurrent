

import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { Platform } from '@ionic/angular';
import { GoogleAuthResponse } from '../app.types';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { ConfSheet } from '../models/confSheet';
import { Order } from '../models/order';
import { Configuration } from '../models/configuration';
import * as firebase from 'firebase';

declare var gapi;
/**
 * @author Quentin San José
 * @description Authentication service for checking and validating user credentials + spreadsheet
 * @deprecated
 */
@Injectable()
export class AuthService {
  private readonly USER: string = 'USER';
  calendarItems: any[];
  confGetCommon: ConfSheet;
  confGapi: any;
  spreadSheetId: string = '';
  constructor(private google: GooglePlus,
    private platform: Platform,
    public afAuth: AngularFireAuth) {


    // conf gapi
    this.confGapi = {
      scope: 'https://www.googleapis.com/auth/spreadsheets',
      apiKey: 'AIzaSyCRC9eka2ujjo51e5q70P4UGhSpzj1_tJ0',
      clientId: '438944116448-2j2gl3a4a3pe71grp37cie67mis9112s.apps.googleusercontent.com',
      discoveryDocs: ['https://sheets.googleapis.com/$discovery/rest?version=v4']
    };

    // spreadsheet id
    this.spreadSheetId = '1Fnp3fvF_V9C_BACSD5dpkbzgfpJGNBJXoVxwtY5Qvfk';

    this.initClient();
  }

  /**
   * Initialize the Google API client with desired scopes  
   */
  initClient() {
    const client = this.platform.is('cordova') ? 'client:auth2' : 'client';
    gapi.load(client, () => {
      gapi.client.init(this.confGapi).then(() => {
        if (this.getUser() !== null && this.platform.is('cordova')) {
          gapi.client.setToken({ access_token: this.getUser().accessToken })
        }
        gapi.client.load('sheets', 'v4', () => {
          gapi.auth2.getAuthInstance().isSignedIn.listen(this.updateSigninStatus);
          // Handle the initial sign-in state.
          this.updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        }), err => console.error(err)
      })
    });
  }

  updateSigninStatus(isSignedIn: boolean) {
    if (isSignedIn) {
      console.log('signed in') 
    } else {
      console.error('nont signed in')
    }
  }

  /**
   * Init Google Authentication
   */
  public googleAuthentication(): Observable<GoogleAuthResponse> {
    if (this.platform.is('cordova')) {
      return Observable.create(observer => {
        this.google.login({
          webClientId: '438944116448-2j2gl3a4a3pe71grp37cie67mis9112s.apps.googleusercontent.com',
          offline: true,
          scopes: 'profile email https://www.googleapis.com/auth/spreadsheets'
        }).then(user => {
          const { idToken, accessToken } = user;
          const credential = accessToken ? firebase.auth.GoogleAuthProvider
            .credential(idToken, accessToken) : firebase.auth.GoogleAuthProvider
              .credential(idToken);
          this.afAuth.auth.signInWithCredential(credential)
            .then(response => {
              observer.next(user);
              this.setUser(user);
            }).catch(error => {
              observer.error(error);
            });
        });
      });
    } else {
      this.loginWeb();
    }
  }

  /**
   * Google Web Login
   */
  loginWeb() {
    const googleAuth = gapi.auth2.getAuthInstance();
    return googleAuth.signIn(this.confGapi).then(googleUser => {
      const token = googleUser.getAuthResponse().id_token;
      const credential = firebase.auth.GoogleAuthProvider.credential(token);
      return this.afAuth.auth.signInAndRetrieveDataWithCredential(credential).then(data => {
        this.setUser(data.user);
        setTimeout(_ => location.reload(), 200);
        return data;
      });
    }, err => console.log(err));
  }



  /**
   * Retrieves user
   */
  public getUser(): GoogleAuthResponse {
    return JSON.parse(localStorage.getItem(this.USER));
  }

  /**
   * Logs user out from the connection
   */
  public logOutGoogle(): Observable<any> {
    if (this.platform.is('cordova')) {
      return Observable.create(observer => {
        this.afAuth.auth.signOut().then(_ => {
          observer.next({});
          this.removeUser();
        }).catch(err => {
          observer.next({});
          this.removeUser();
        });
      });
    } else {
      const googleAuth = gapi.auth2.getAuthInstance();
      return Observable.create(observer => {
        googleAuth.signOut().then(googleUser => {
          observer.next({});
          this.removeUser();
        });
      });
    }
  }

  /**
   * Sets user in storage
   * @param user 
   */
  private setUser(user) {
    localStorage.setItem(this.USER, JSON.stringify(user));
  }

  /**
   * Clear user info
   */
  private removeUser() {
    localStorage.removeItem(this.USER);
  }

  /*****************************************
   *************** SPREADSHEETS ************
   *****************************************
   *****************************************/



}
