import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'current',
        children: [
          {
            path: '',
            loadChildren: '../current/current.module#CurrentPageModule'
          }
        ]
      },
      {
        path: 'currents',
        children: [
          {
            path: '',
            loadChildren: '../currents/currents.module#CurrentsPageModule'
          }
        ]
      },
      {
        path: 'previous',
        children: [
          {
            path: '',
            loadChildren: '../previous/previous.module#PreviousPageModule'
          }
        ]
      },
      {
        path: 'next',
        children: [
          {
            path: '',
            loadChildren: '../next/next.module#NextPageModule'
          }
        ]
      },
      {
        path: 'postponed',
        children: [
          {
            path: '',
            loadChildren: '../postponed/postponed.module#PostponedPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: 'tabs/current',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'tabs/current',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
