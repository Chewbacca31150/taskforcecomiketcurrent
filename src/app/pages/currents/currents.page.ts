import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { DayComponent } from 'src/app/components/day/day.component';
import { SpreadSheetService } from 'src/app/services/spreadsheet.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-currents',
  templateUrl: 'currents.page.html',
  styleUrls: ['currents.page.scss']
})
export class CurrentsPage {
  @ViewChild(DayComponent)
  child: DayComponent;
  sheets: string[];
  sheetToUse: string;
  constructor(private spreadSheetService: SpreadSheetService, 
    private utilsService: UtilsService,
    private cdr: ChangeDetectorRef) {}

  ionViewWillEnter() {
    this.getSheets();
  }

  ionViewWillLeave() {
    this.utilsService.clearIntervals('app');
  }

  getSheets() {
    this.spreadSheetService.getAllSheets().then(data => {
      this.sheets = data;
      this.cdr.detectChanges();
    });
  }

  onChange(event) {
    this.utilsService.clearIntervals('app');
    // I think we have to avoid this but it works like a charm o/
    this.child.setOrders(null);
    this.child.setSheetToUse(this.sheetToUse);
    this.child.initPage();
  }

  doRefresh(event) {
    setTimeout(() => {
      this.child.getData(null).then(_ => event.target.complete());
    }, 2000);
  }
}
