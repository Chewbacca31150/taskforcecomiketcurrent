import { Component, ChangeDetectorRef } from '@angular/core';
import { Order } from '../../models/order';

import { ModalController, LoadingController, PopoverController } from '@ionic/angular';
import _ from 'lodash';
import { DetailsModal } from '../../components/details/details.modal';
import { SpreadSheetService } from 'src/app/services/spreadsheet.service';
import { UtilsService } from 'src/app/services/utils.service';
import { DoneComponent } from 'src/app/components/done/done.component';

@Component({
  selector: 'app-current',
  templateUrl: 'current.page.html',
  styleUrls: ['current.page.scss']
})
export class CurrentPage {
  current: Order;
  token: string;
  error: string;
  constructor(
    public utilsService: UtilsService,
    public spreadsheetService: SpreadSheetService,
    private loadingController: LoadingController,
    private cdr: ChangeDetectorRef,
    private modalController: ModalController,
    private popoverController: PopoverController) {
  }

  ionViewWillEnter() {
    this.current = null;
    this.initPage();
  }

  ionViewWillLeave() {
    this.utilsService.clearIntervals('app');
  }

  initPage() {
    if (!this.current) this.loadFirstTime();
    this.loadData();
  }

  refreshData() {
    this.getCurrent(null);
    this.loadData();
  }

  /**
   * Retrieves data each 10sec
   */
  loadData() {
    const id = window.setInterval(() => this.getCurrent(null), 10000);
    this.utilsService.addInterval({type: 'app', id});
  }

  /**
   * Displays the spinner
   */
  loadFirstTime() {
    let loading: Promise<HTMLIonLoadingElement> = null;
    loading = this.loadingController.create({
      spinner: "bubbles",
    });
    loading.then(load => load.present());
    setTimeout(() => this.getCurrent(loading), 2000);
  }

  /**
   * Get the current from spreadsheet
   * @param isFirst boolean
   */
  getCurrent(loading) {
    return this.spreadsheetService.getOrdersByOwner().then((data: Order[]) => {
      this.removeLoader(loading);
      if (!data) return;
      const current = data.filter((d: Order) => d.current == 0).shift();
      if (current && JSON.stringify(current) === JSON.stringify(this.current)) return;
      const wasNull: boolean = this.current == null;
      this.current = current;
      if (!this.current) this.error = 'No entry found yet';
      if(wasNull) this.reloadCdr();
      return this.current;
    }, _ => {
      this.error = 'Network error';
      this.removeLoader(loading);
    });
  }

  /**
   * Remove the loader if needed
   * @param loading 
   */
  removeLoader(loading) {
    if (loading !== undefined && loading !== null) loading.then(load => load.remove());
  }

  /**
   * Force reload scope if needed
   * @param loading 
   */
  reloadCdr() {
    this.cdr.detectChanges();
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getCurrent(null).then(_ => event.target.complete());
    }, 2000);
  }

  /**
   * Go to details to update the order
   * @param order 
   */
  async goToDetails() {
    const order = this.current;
    const modal = await this.modalController.create({
      component: DetailsModal,
      componentProps: {
        order
      }
    });
    modal.onDidDismiss().then(_ => this.loadData());
    modal.present()
      .then(() => {
        this.utilsService.clearIntervals('app');
      });
  }

  async showDoneButtons(ev: any) {
    const popover = await this.popoverController.create({
      component: DoneComponent,
      event: ev,
      translucent: true,
      componentProps: {
        current: this.current
      }
    });
    popover.present();
    popover.onDidDismiss().then(() => {
      this.current = null;
      this.cdr.detectChanges();
      this.refreshData();
    });
  }

}
