import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Configuration } from '../../models/configuration';
import { LoadingController } from '@ionic/angular';
import { SpreadSheetService } from 'src/app/services/spreadsheet.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.page.html',
  styleUrls: ['./configuration.page.scss'],
})
export class ConfigurationPage implements OnInit {

  constructor(private spreadSheetService: SpreadSheetService, 
    private loadingController: LoadingController, 
    private utilsService: UtilsService,
    private cdr: ChangeDetectorRef) { }
  config: Configuration;
  ngOnInit() {
    //this.utilsService.clearIntervals('app');
    let loading: Promise<HTMLIonLoadingElement> = this.loadingController.create({
      spinner: "bubbles",
   });
   loading.then(load => load.present());
    setTimeout(() => this.readConf(loading), 2000);
  }

  readConf(loading: Promise<HTMLIonLoadingElement>) {

   this.spreadSheetService.getConfig().then(data => {
      this.config = data;
      this.cdr.detectChanges();
      loading.then(load => load.remove());
   });
  }

}
