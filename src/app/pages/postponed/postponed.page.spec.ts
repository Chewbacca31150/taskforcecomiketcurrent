import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostponedPage } from './postponed.page';

describe('PostponedPage', () => {
  let component: PostponedPage;
  let fixture: ComponentFixture<PostponedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostponedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostponedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
