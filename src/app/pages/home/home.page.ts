import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { AuthAndroidService } from 'src/app/services/auth.android.service';
import { AuthWebService } from 'src/app/services/auth.web.service';
import { AuthAbstractService } from 'src/app/services/auth.abstract.service';
import { Platform } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
/**
 * @author Quentin San José
 * @description Home page , which allow the user to initiate Google 
 * authentication
 */
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  constructor(
    private utilsService: UtilsService,
    private route: Router) {

    // Retrive user from local storage
    if (!this.utilsService.getAuthInstance().getUser()) return;
    this.openTabs();
  }

  public googleAuth() {
    // Init Google Auth
    this.utilsService.getAuthInstance().googleAuthentication();
  }

  // Sets tabs
  private openTabs() {
    this.route.navigate(['/tabs']).then(_ => window.location.reload());
  }
}
