import { Component, ViewChild } from '@angular/core';
import { DayComponent } from 'src/app/components/day/day.component';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-previous',
  templateUrl: 'previous.page.html',
  styleUrls: ['previous.page.scss']
})
export class PreviousPage {
  @ViewChild(DayComponent)
  child: DayComponent;
  constructor(private utilsService: UtilsService) {}

  ionViewWillEnter() {
    this.child.initPage();
  }

  ionViewWillLeave() {
    this.utilsService.clearIntervals('app');
  }

  doRefresh(event) {
    setTimeout(() => {
      this.child.getData(null).then(_ => event.target.complete());
    }, 2000);
  }
}
