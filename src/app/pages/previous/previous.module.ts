import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PreviousPage } from './previous.page';
import { SharedModule } from '../../components/shared.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: PreviousPage }]),
    SharedModule
  ],
  declarations: [PreviousPage]
})
export class PreviousPageModule {}
