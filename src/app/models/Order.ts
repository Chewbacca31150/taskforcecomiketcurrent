export class Order {
    hall: string;
    booth: string;
    circle: string;
    what: string;
    for: string;
    whoGetsIt: string;
    priceExpected: number;
    priceReal: number;
    website: string;
    image: string;
    current: number;
    note: string;
    noteDone: string;
    row: string;
    status: string;
    priority: number;
    dateDone: Date;
}