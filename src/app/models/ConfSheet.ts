export class ConfSheet {
    sheet: string;
    startingColumn: string;
    startingRange: number;
    endRange: string;
}
