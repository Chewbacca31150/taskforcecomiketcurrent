# INSTALLATION

- install ionic
`npm i -g ionic`

- install dependencies
`npm i`

- run on browser
`ionic serve`

# To deploy the app
## Generate keystore
`keytool -genkey -v -keystore my-release-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 100000`
## Build apk
`ionic cordova build android --prod --release`
## Generate digital signatures
`jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore deploy/debug.keystore platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk my-key-alias`
## To align
`zipalign -v 4 platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk deploy/taskforcecomiketalpha.apk`
@Chewie `~/Library/Android/sdk/build-tools/29.0.1/zipalign -v 4 platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk deploy/taskforcecomiketalpha.apk`
AND last but not least, copy pasta your apk on your phone

For deployment purpose on the play store: TODO

# If you use several jdk on your environment... (for mac only)
`alias j8="export JAVA_HOME=`/usr/libexec/java_home -v 1.8`; java -version"`
`j8`
Then run your commands..

# DRAFTS